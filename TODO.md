- [ ] Logger
  - [x] Private messages
    - [x] Log new messages
    - [x] Log edits to messages
    - [x] Log pins in channel
  - [x] Guild messages
    - [x] Log new messages
    - [x] Log edits to messages
    - [x] Log pins in channel
   - [ ] Log channel changes
   - [ ] Log guild changes
   - [ ] Log role changes
   - [x] Log user updates
     - [x] Log role additions
     - [x] Log role removals
  - [ ] Log old/existing messages
- [ ] Viewer
  - [x] Template
  - [x] DM list
  - [x] Guild overview
  - [x] Guild details
  - [x] Channel list
  - [ ] Channel info
  - [x] Date/time per channel
  - [ ] Messages per day
    - [x] Message content
    - [x] Attachments
    - [x] Embeds
    - [ ] Export as panopticon logs
  - [x] User overview
    - [ ] User guild history
  - [ ] Role details
  - [ ] Search function

Optional features:

If implemented, these should be toggleable due to storage concerns.
- [x] Localize attachments
- [x] Localize user icons

Deployment things:

- [ ] Docker?

Really far future stuff:

- [ ] Logging reactions
  - [x] Implement on template
  - [ ] Implement in logger
  - [ ] Implement in viewer 